package tp_tod;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {

    Fraction frac1, frac2;

    @Before
    public void setUp() {
        frac1 = new Fraction(4, 2);
        frac2 = new Fraction(6, 3);
    }

    @Test
    public void testEgalite()
    {
        assertTrue("égalité entre 4/2 et 6/3", frac1.value() == frac2.value());
    }
}
package tp_tod;

public class Fraction implements Comparable<Fraction> {

    private final int  numerateur;
    private final int  denominateur;
    final public static Fraction ZERO = new Fraction(0, 1);
    final public static Fraction UN = new Fraction(1, 1);

    public Fraction(int d,int n)
    {
        numerateur = n;
        denominateur = d;
    }
    public Fraction(int n)
    {
        numerateur = n;
        denominateur = 1;
    }
    public Fraction()
    {
        numerateur = 0;
        denominateur = 1;
    }

    public int get_den()
    {
        return denominateur;
    }
    public int get_num()
    {
        return numerateur;
    }
    public double value()
    {
        return numerateur/denominateur;
    }
    
    public int compareTo(Fraction other){
        if(this.value() < other.value())
            return -1;
        else if(other.value() < this.value())
            return 1;
        return 0;
	}
    
    @Override
    public String toString() {
        return numerateur + "/" + denominateur;
    }
    

}
